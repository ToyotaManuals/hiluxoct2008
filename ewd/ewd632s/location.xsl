<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<HEAD>
				<META http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
				<META http-equiv="Content-Style-Type" content="text/css" />
				<LINK REL="STYLESHEET" TYPE="text/css" HREF="menu.css"/>
				<script type="text/javascript">
					var bSelectedID = "";

					function selectItem(idSel)
					{
						nID = "id" + idSel;
						if( bSelectedID != "" ){	
							document.all(bSelectedID).className = "item";
						}
						document.all(nID).className = "selectedItem";
						bSelectedID = nID;
					}
				</script>
			</HEAD>
			<body>
				<xsl:apply-templates select="ItemList"/>
			</body>
		</html>

	</xsl:template>

	<xsl:template match="ItemList">
		<font size="-1"><b><xsl:value-of select="@sysname" /></b></font>
		<div class="itemList">
			<xsl:for-each select="item">
					<xsl:variable name="i" select="position()" />
					<xsl:if test="@type='conn'">
						<xsl:if test="@no='1'">
							<div class="showType">Localización de partes</div>
						</xsl:if>
					</xsl:if>
					<xsl:if test="@type='rb'">
						<xsl:if test="@no='1'">
							<div class="showType">Bloques de relés</div>
						</xsl:if>
					</xsl:if>
					<xsl:if test="@type='jb'">
						<xsl:if test="@no='1'">
							<div class="showType">Bloques de enlaces</div>
						</xsl:if>
					</xsl:if>
					<xsl:if test="@type='w2w'">
						<xsl:if test="@no='1'">
							<div class="showType">Alambre y Alambre</div>
						</xsl:if>
					</xsl:if>
					<xsl:if test="@type='gp'">
						<xsl:if test="@no='1'">
							<div class="showType">Puntos a tierra</div>
						</xsl:if>
					</xsl:if>
					<xsl:if test="@type='sp'">
						<xsl:if test="@no='1'">
							<div class="showType">Puntos de la unión</div>
						</xsl:if>
					</xsl:if>
					<div class="item">
						<A class="item" target="PDF_Frame">
							<xsl:attribute name="id">id<xsl:value-of select="$i"/></xsl:attribute>
							<xsl:attribute name="onclick">selectItem(<xsl:value-of select="$i"/>)</xsl:attribute>

						<xsl:choose>
							<xsl:when test="@type='w2w'">

							<xsl:attribute name="href"><xsl:value-of select="ref/@type"/>/w2w.html?page=<xsl:value-of select="ref/@page"/></xsl:attribute>
							</xsl:when>
							<xsl:when test="@type='jb' or @type='rb'">
							<xsl:attribute name="href"><xsl:value-of select="ref/@type"/>/relay.html?page=<xsl:value-of select="ref/@page"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>

							<xsl:attribute name="href"><xsl:value-of select="ref/@type"/>/<xsl:value-of select="ref/@page"/>.pdf</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>


							<xsl:value-of select="@code"/>
							<xsl:if test="note!=''">
								 &lt;<xsl:value-of select="note"/>&gt;
							</xsl:if>
						</A>
					</div>
			</xsl:for-each>
		</div>
	</xsl:template>

</xsl:stylesheet>
